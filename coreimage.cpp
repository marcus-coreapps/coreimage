﻿/*
    *
    * This file is a part of CoreImage.
    * An image viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QListWidgetItem>
#include <QStandardPaths>
#include <QImageReader>
#include <QImageWriter>
#include <QFileInfo>
#include <QShortcut>
#include <QTimer>
#include <QScreen>
#include <QDebug>
#include <QCollator>

#include <cprime/themefunc.h>
#include <cprime/cprime.h>
#include <cprime/appopenfunc.h>
#include <cprime/pinit.h>
#include <cprime/trashmanager.h>
#include <cprime/shareit.h>

#include "coreimage.h"
#include "ui_coreimage.h"


coreimage::coreimage(QWidget *parent) : QWidget(parent), ui(new Ui::coreimage)
  , workFilePath("")
  , slideShow(false)
  , slideShowTimer(nullptr)
  , smi(new settings)

{
    ui->setupUi(this);

    loadSettings();
	startSetup();
    setupIcons();
    startMode();
    shortcuts();
	getImageFilters();
}

coreimage::~coreimage()
{
    if (slideShowTimer) {
		delete slideShowTimer;
    }

	delete smi;
	delete ui;
}


/**
 * @brief Loads application settings
 */
void coreimage::loadSettings()
{
    // get CSuite's settings
    uiMode = smi->getValue("CoreApps", "UIMode");
    activities = smi->getValue("CoreApps", "KeepActivities");
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");
    disableTrashConfirmationMessage  = smi->getValue("CoreApps", "DisableTrashConfirmationMessage");

    // get app's settings
    initialZoomMode = smi->getValue("CoreImage", "InitialZoomMode");
    sortMode = smi->getValue("CoreImage", "SortMode");
    windowSize = smi->getValue("CoreImage", "WindowSize");
    qDebug() << windowSize;
    windowMaximized = smi->getValue("CoreImage", "WindowMaximized");
}

/**
 * @brief Setup ui elements
 */
void coreimage::startSetup()
{
    QPalette pltt(palette());
    pltt.setColor(QPalette::Base, Qt::transparent);
    setPalette(pltt);

    imagesList.clear();
    openFilter.clear();
    saveFilter.clear();

    // Zoom label
	zoomLbl = new QLabel(this);
    zoomLbl->setStyleSheet("QLabel{ margin: 5px; padding: 5px; background-color: rgba(0, 0, 0, 0.5); border-radius: 3px; }");
    zoomLbl->setText("Zoom: 1.0x");
    ui->gridLayout->addWidget(zoomLbl, 1, 2, 1, 1, Qt::AlignLeft | Qt::AlignTop);

    // all toolbuttons icon size in sideView
    QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns) {
        if (b) {
            b->setIconSize(toolsIconSize);
        }
    }

    // all toolbuttons icon size in toolBar
    QList<QToolButton *> toolBtns2 = ui->toolBar->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns2) {
        if (b) {
            b->setIconSize(toolsIconSize);
        }
    }

    ui->propertisesL->setVisible(true);
    ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
    ui->appTitle->setFocusPolicy(Qt::NoFocus);
    this->resize(800, 600);

    // QCoreApplication::processEvents();

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);

        ui->activitiesList->setVisible(false);
        ui->propertisesL->setVisible(false);
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }

        if(activities){
            loadActivities();
        }
    }

    // QCoreApplication::processEvents();

    ui->page->setCurrentIndex(0);

    connect(ui->viewWidget, &view::zoomChanged, this, &coreimage::changeZoomLabel);
    connect(ui->cOpen, SIGNAL(clicked()), this, SLOT(openFileDialog()));
    connect(ui->addFiles, SIGNAL(clicked()), this, SLOT(openFileDialog()));
    connect(ui->viewWidget, SIGNAL(nextImage()), this, SLOT(nextItem()));
    connect(ui->viewWidget, SIGNAL(prevImage()), this, SLOT(previousItem()));
    connect(ui->cNext, SIGNAL(clicked()), this, SLOT(nextItem()));
    connect(ui->cPrevious, SIGNAL(clicked()), this, SLOT(previousItem()));
    connect(ui->cZoomIn, SIGNAL(clicked()), ui->viewWidget, SLOT(zoomIn()));
    connect(ui->cZoomOut, SIGNAL(clicked()), ui->viewWidget, SLOT(zoomOut()));
    connect(ui->menu, SIGNAL(clicked()), this, SLOT(showSideView()));
    connect(ui->appTitle, SIGNAL(clicked()), this, SLOT(showSideView()));
    connect(ui->sortOrder, SIGNAL( clicked() ), this, SLOT( nextSortMode() ) );
}

void coreimage::setupIcons()
{
    ui->cOpen->setIcon(CPrime::ThemeFunc::themeIcon( "document-open-symbolic", "quickopen-file", "document-open-symbolic" ));
    ui->pinIt->setIcon(CPrime::ThemeFunc::themeIcon( "bookmark-new-symbolic", "bookmark-new-symbolic", "bookmark-new" ));
    ui->shareIt->setIcon(CPrime::ThemeFunc::themeIcon( "document-send-symbolic", "document-send-symbolic", "document-send" ));
    ui->cPrevious->setIcon(CPrime::ThemeFunc::themeIcon("go-previous-symbolic", "go-previous", "go-previous" ));
    ui->cNext->setIcon(CPrime::ThemeFunc::themeIcon("go-next-symbolic", "go-next", "go-next" ));
    ui->containingFolder->setIcon(CPrime::ThemeFunc::themeIcon( "document-open-symbolic", "quickopen-file", "document-open-symbolic" ));
    ui->openInEditor->setIcon(CPrime::ThemeFunc::themeIcon( "document-edit-symbolic", "edit-rename", "document-edit" ));
    ui->openmetadataApp->setIcon(CPrime::ThemeFunc::themeIcon( "document-properties-symbolic", "document-properties", "document-properties" ));
    ui->cSave->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-symbolic", "document-save", "document-save" ));
    ui->cSaveAs->setIcon(CPrime::ThemeFunc::themeIcon( "document-save-as-symbolic", "document-save-as", "document-save-as" ));
    ui->menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
    ui->slideShow->setIcon(CPrime::ThemeFunc::themeIcon( "media-playback-start-symbolic", "media-playback-start", "media-playback-start" ));
    ui->cZoomIn->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-in-symbolic", "zoom-in", "zoom-in" ));
    ui->cZoomOut->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-out-symbolic-symbolic", "zoom-out", "zoom-out" ));
    ui->cAlternateZoom->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-fit-best-symbolic", "zoom-fit-best", "zoom-fit-best" ));
    ui->rotate->setIcon(CPrime::ThemeFunc::themeIcon( "object-rotate-right-symbolic", "object-rotate-right", "object-rotate-right" ));
    ui->cTrashIt->setIcon(CPrime::ThemeFunc::themeIcon( "user-trash-symbolic", "edit-delete", "edit-delete" ));

    // set sort button icon as it depends on the sort order
    setSortMode();
}

void coreimage::loadActivities()
{
    if(not activities){
        ui->activitiesList->setVisible(false);
        return;
    }

    ui->activitiesList->clear();

    QSettings recentActivity(CPrime::Variables::CC_ActivitiesFilePath(), QSettings::IniFormat);
    QStringList topLevel = recentActivity.childGroups();

    if (topLevel.length()) {
        topLevel = CPrime::SortFunc::sortDate(topLevel, Qt::DescendingOrder);
    }

    QListWidgetItem *item;

    Q_FOREACH (QString group, topLevel) {
        recentActivity.beginGroup(group);
        QStringList keys = recentActivity.childKeys();
        keys = CPrime::SortFunc::sortTime(keys, Qt::DescendingOrder, "hh.mm.ss.zzz");

        Q_FOREACH (QString key, keys) {
            QStringList value = recentActivity.value(key).toString().split("\t\t\t");
            if (value[0] == "coreimage") {
                QString filePath = value[1];
                if (not CPrime::FileUtils::exists(filePath))
                    continue;

                QString baseName = CPrime::FileUtils::baseName(filePath);
                item = new QListWidgetItem(baseName);
                item->setData(Qt::UserRole, filePath);
                item->setToolTip(filePath);
                item->setIcon(CPrime::ThemeFunc::getFileIcon(filePath));
                ui->activitiesList->addItem(item);
            }
        }

        recentActivity.endGroup();
    }
}

void coreimage::startMode()
{
    enableButtons(false);

    ui->cOpen->setEnabled(true);
    ui->menu->setEnabled(true);
    ui->sideView->setVisible(false);
}

void coreimage::shortcuts()
{
	QShortcut *shortcut;
    shortcut = new QShortcut(QKeySequence(Qt::Key_Up) , ui->app);
    connect(shortcut, &QShortcut::activated, this, &coreimage::previousItem);

    shortcut = new QShortcut(QKeySequence(Qt::Key_Down), ui->app);
    connect(shortcut, &QShortcut::activated, this, &coreimage::nextItem);

    shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_O), this);
    connect(shortcut, &QShortcut::activated, this, &coreimage::openFileDialog);

    shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_S), ui->app);
    connect(shortcut, &QShortcut::activated, this, &coreimage::on_cSave_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::ShiftModifier | Qt::Key_S), ui->app);
    connect(shortcut, &QShortcut::activated, this, &coreimage::on_cSaveAs_clicked);

    shortcut = new QShortcut(QKeySequence::fromString( smi->getValue( "CoreStuff", "DockKeyCombo" ) ), ui->app);
    connect(shortcut, &QShortcut::activated, this, &coreimage::on_cTrashIt_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_Plus), ui->app);
    connect(shortcut, &QShortcut::activated,  ui->viewWidget, &view::zoomIn);

    shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_Minus), ui->app);
    connect(shortcut, &QShortcut::activated,  ui->viewWidget, &view::zoomOut);

    shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_0), ui->app);
    connect(shortcut, &QShortcut::activated,  ui->viewWidget, &view::zoomNormal);

    shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_B), ui->app);
    connect(shortcut, &QShortcut::activated, this, &coreimage::on_pinIt_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_Escape), ui->app);
    connect(shortcut, &QShortcut::activated, this, &coreimage::closeSlideShow);
}

void coreimage::sendFiles(const QStringList &paths)
{
    if (paths.length()) {
        QString str = paths.first();

        if (CPrime::FileUtils::isFile(str)) {
            imagesList.clear();
            workFilePath = str;
            getImagesForOpenedImage();
            loadImage(str);
            enableButtons(true);

            // qDebug () << this->size() ;
            // QSize currentWindowSize = this->size();
            // this->resize(currentWindowSize - QSize(20,20));
            // ui->viewWidget->zoomNormal();
            // ui->viewWidget->resize(currentWindowSize);
            //
            // qDebug () << this->size() ;
            // if(initialZoomMode == true){
            //     ui->viewWidget->zoomNormal();
            // } else {
            //     ui->viewWidget->zoomFit();
            // }
            // this->resize(currentWindowSize);
            //
            // qDebug () << this->size() ;
        } else {
            qDebug() << "func(sendFiles) : Error : Invalid file path sent :" << str;
        }
    }
}

bool coreimage::loadImage(QString filePath)
{
    bool imageLoaded = false;

    if (!filePath.length()) {
        CPrime::MessageEngine::showMessage("org.cubocore.CoreImage", "CoreImage", "Empty file name!!!", "Select a valid file");
        return imageLoaded;
    }

    if (ui->page->currentIndex() != 1) {
        ui->page->setCurrentIndex(1);
    }

    imageLoaded = ui->viewWidget->loadImage(filePath);
    QString fileName = QFileInfo(filePath).fileName();

    if (imageLoaded) {
        workFilePath = filePath;

        int total = imagesList.length();
        int index = imagesList.indexOf(filePath);

        QString fileName = QFileInfo(filePath).fileName();
        this->setWindowTitle(fileName + " - (" + QString::number(index+1) + "/" + QString::number(total)+ ") - CoreImage");

        if(initialZoomMode == true){
            ui->viewWidget->zoomNormal();
        } else {
            ui->viewWidget->zoomFit();
        }

        if (uiMode != 2) {
            QFileInfo info(filePath);
            QString n = info.fileName();
            QString h = QString::number(ui->viewWidget->asImage().height()) ;
            QString w = QString::number(ui->viewWidget->asImage().width()) ;
            QString s = CPrime::FileUtils::formatSize(static_cast<quint64>(info.size()));
            QString typ = info.suffix();
            ui->propertisesL->setText(QString("%1; %2 px x %3 px; %4; %5;").arg(n, w, h, s, typ));
        }

    } else {
        QString mess = tr("Could not load image <b>%1</b>").arg(fileName) ;
        CPrime::MessageEngine::showMessage("org.cubocore.CoreImage", "CoreImage", "Failed to load", mess);
    }

    return imageLoaded;
}

void coreimage::getImageFilters()
{
    openFilter.clear();
    saveFilter.clear();

    // build open filter
    // make "all supported" part
    QList<QByteArray> ba = QImageReader::supportedImageFormats();
    openFilter = "All supported (";

    foreach (QByteArray temp, ba) {
        openFilter += "*." + temp + " ";}

    openFilter[openFilter.length() - 1] = ')'; //delete last space

    Q_FOREACH (QByteArray type, QImageWriter::supportedImageFormats()) {
        openFilter += QString(";;%1 File (*.").arg(QString(type.toUpper())) + type + ")";
    }

    // build save filter
    Q_FOREACH (QByteArray type, QImageWriter::supportedImageFormats()) {
        saveFilter += QString(";;%1 File (*.").arg(QString(type.toUpper())) + type + ")";
    }

}

void coreimage::enableButtons(bool enable)
{
    if(!enable){
        zoomLbl->setText("No Image file");
    }
    QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns) {
        b->setEnabled(enable);
    }

    QList<QToolButton *> toolBtns2 = ui->toolBar->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns2) {
        b->setEnabled(enable);
    }
}

void coreimage::setSortMode()
{
    /* Sort by Name */
    if ( sortMode == 0 ){
        ui->sortOrder->setText("Sort by Name");
        ui->sortOrder->setIcon(CPrime::ThemeFunc::themeIcon( "view-sort-ascending-symbolic", "view-sort-ascending", "view-sort-ascending"));
        ascending = true;
    } else if ( sortMode == 1 ){
        ui->sortOrder->setText("Sort by Name");
        ui->sortOrder->setIcon(CPrime::ThemeFunc::themeIcon( "view-sort-descending-symbolic", "view-sort-descending", "view-sort-descending"));
        ascending = false;
    }

    /* Sort by Size */
    else if ( sortMode == 2 ){
        ui->sortOrder->setText("Sort by Size");
        ui->sortOrder->setIcon(CPrime::ThemeFunc::themeIcon( "view-sort-ascending-symbolic", "view-sort-ascending", "view-sort-ascending"));
        ascending = true;
    } else if ( sortMode == 3 ){
        ui->sortOrder->setText("Sort by Size");
        ui->sortOrder->setIcon(CPrime::ThemeFunc::themeIcon( "view-sort-descending-symbolic", "view-sort-descending", "view-sort-descending"));
        ascending = false;
    }

    /* Sort by Time */
    else if ( sortMode == 4 ){
        ui->sortOrder->setText("Sort by Time");
        ui->sortOrder->setIcon(CPrime::ThemeFunc::themeIcon( "view-sort-ascending-symbolic", "view-sort-ascending", "view-sort-ascending"));
        ascending = true;
    } else if ( sortMode == 5 ){
        ui->sortOrder->setText("Sort by Time");
        ui->sortOrder->setIcon(CPrime::ThemeFunc::themeIcon( "view-sort-descending-symbolic", "view-sort-descending", "view-sort-descending"));
        ascending = false;
    }

    /* Sort by Type */
    else if ( sortMode == 6 ){
        ui->sortOrder->setText("Sort by Type");
        ui->sortOrder->setIcon(CPrime::ThemeFunc::themeIcon( "view-sort-ascending-symbolic", "view-sort-ascending", "view-sort-ascending"));
        ascending = true;
    } else if ( sortMode == 7 ){
        ui->sortOrder->setText("Sort by Type");
        ui->sortOrder->setIcon(CPrime::ThemeFunc::themeIcon( "view-sort-descending-symbolic", "view-sort-descending", "view-sort-descending"));
        ascending = false;
    }
}

void coreimage::nextSortMode()
{
    // increase the sortMode number
    sortMode = ( sortMode + 1 ) % 8;
    // set sort button icon
    setSortMode();
    // save the current file path, after sort so app can start directly from there
    QString curerntFile = workFilePath;

    // clear the list and generate new list
    imagesList.clear();
    getImagesForOpenedImage();

    // locate the old file in the list and set the current index to that
    if (imagesList.contains(curerntFile)) {
        // qDebug() << "Match found!"<< imagesList.indexOf(curerntFile);
        workFilePath = curerntFile;
    }

    smi->setValue("CoreImage", "SortMode", sortMode);
};

void coreimage::getImagesForOpenedImage()
{
    if (!workFilePath.length()) {
        return;
    }

    QFileInfo fileInfo(workFilePath);
    QDir dir(fileInfo.path());

    if (!dir.exists()) {
        return;
    }

    if (imagesList.length()) {
        return;
    }

    QList<QByteArray> imageMime = QImageReader::supportedImageFormats();

    // Create a list to store the fileInfo and sort criteria values
    QList<QPair<QFileInfo, QVariant>> fileSortCriteria;

    auto fileInfos = dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot | QDir::Readable);
    for (const QFileInfo &info : fileInfos) {
        if (imageMime.contains(info.suffix().toLocal8Bit().toLower())) {
            qApp->processEvents();
            QVariant sortValue;

            if (sortMode == 0 || sortMode == 1) { // sort by name
                sortValue = info.fileName();
            } else if (sortMode == 2 || sortMode == 3) { // sort by size
                sortValue = info.size();
            } else if (sortMode == 4 || sortMode == 5) { // sort by time
                sortValue = info.lastModified();
            } else if (sortMode == 6 || sortMode == 7) { // sort by type
                sortValue = info.completeSuffix();
            } else {
                // Invalid sort option
                return;
            }

            fileSortCriteria.append(qMakePair(info, sortValue));
        }
    }


    // Sort the fileSortCriteria list based on the sort criteria values using QCollator
    QCollator collator;
    collator.setNumericMode(true); // Enable numeric mode for sorting by size or time

    if (!ascending) {
        collator.setCaseSensitivity(Qt::CaseSensitive); // Set case sensitivity for descending order
    }

    std::sort(fileSortCriteria.begin(), fileSortCriteria.end(), [this, &collator](const QPair<QFileInfo, QVariant> &a, const QPair<QFileInfo, QVariant> &b) {
        int result = collator.compare(a.second.toString(), b.second.toString());
        return ascending ? result < 0 : result > 0;
    });



    // Extract the sorted file paths from the fileSortCriteria list into imagesList
    for (const QPair<QFileInfo, QVariant> &fileCriteria : fileSortCriteria) {
        imagesList.append(fileCriteria.first.filePath());
    }
}

void coreimage::on_cSave_clicked()
{
    if (ui->viewWidget->saveImage()) {
		CPrime::MessageEngine::showMessage("org.cubocore.CoreImage", "CoreImage", "Image Saved", "Check the destination for the file.");
	} else {
		CPrime::MessageEngine::showMessage("org.cubocore.CoreImage", "CoreImage", "Can't save image", "File is not supported to save.");
	}
}

void coreimage::on_cSaveAs_clicked()
{
    QString filter;

    // see if we have opened files, if so then set the folder path from the file
    QString openFrom = QDir::homePath();
    if (workFilePath.length()) {
        QFileInfo fileInfo(workFilePath);

        if (fileInfo.exists()) {
            openFrom =  workFilePath ;
            workFilePath.clear();
        }
    }

    //make saveFilter
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save image..."), openFrom, saveFilter, &filter);

    //parse file extension
    if (!filePath.isEmpty()) {
        QString extension;
        //we should test it on windows, because of different slashes
        QString temp = filePath.split("/").last();

        //if user entered some extension
        if (temp.contains('.')) {
            temp = temp.split('.').last();

            if (QImageWriter::supportedImageFormats().contains(temp.toLatin1())) {
                extension = temp;
            } else {
                extension = "png";    //if format is unknown, save it as png format, but with user extension
            }
        } else {
            extension = filter.split('.').last().remove(')');
            filePath += '.' + extension;
        }
        qDebug () << "selected" << filePath << extension << extension.toLatin1().data() ;

        if (ui->viewWidget->saveImage(filePath)) {
            CPrime::MessageEngine::showMessage("org.cubocore.CoreImage", "CoreImage", "Image Saved", "Check the destination for the file.");
        } else {
            CPrime::MessageEngine::showMessage("org.cubocore.CoreImage", "CoreImage", "Can't save image", "File is not supported to save.");
        }
    } else {
        CPrime::MessageEngine::showMessage("org.cubocore.CoreImage", "CoreImage", "Cancled save operation", "File path and name is empty.");
    }
}

void coreimage::on_rotate_clicked()
{
    ui->viewWidget->rotateRight();
}

void coreimage::on_openInEditor_clicked()
{
    CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::ImageEditor, QFileInfo( workFilePath ), "");
}

void coreimage::on_openmetadataApp_clicked()
{
    CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::MetadataViewer, QFileInfo( workFilePath ), "");
}

void coreimage::on_containingFolder_clicked()
{
    CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::FileManager,  QFileInfo( CPrime::FileUtils::dirName( workFilePath ) ), "");
}

void coreimage::on_cTrashIt_clicked()
{
    if (!workFilePath.length()) {
        return;
    }

    int index = imagesList.indexOf(workFilePath);

    // Function from utilities.cpp
    if (CPrime::TrashManager::moveToTrash(QStringList() << workFilePath, disableTrashConfirmationMessage) == true) {
    //if (CPrime::TrashManager::moveToTrash(QStringList() << workFilePath) == true) {
        imagesList.removeAt(index);

        if (!imagesList.length()) {
            ui->viewWidget->clear();
            ui->propertisesL->clear();
            startMode();

            this->setWindowTitle("CoreImage");
        } else if (imagesList.length() > 0) {
            if (imagesList.length() == 1) {
                workFilePath = imagesList.at(0);
                loadImage(workFilePath);
            } else if (index == imagesList.length() - 1) {
                workFilePath = imagesList.at(index - 1);
                previousItem();
            } else {
                workFilePath = imagesList.at(index - 1);
                nextItem();
            }
        }
    }
}

void coreimage::on_shareIt_clicked()
{
    if (workFilePath.length()) {
        ShareIT *t = new ShareIT(QStringList() << workFilePath, listViewIconSize, nullptr);
        // Set ShareIT window size
        if (uiMode == 2 )
            t->setFixedSize(QGuiApplication::primaryScreen()->size() * .8 );
        else
            t->resize(500,600);

        t->exec();
    }
}

void coreimage::on_pinIt_clicked()
{
    if (workFilePath.length()) {
        PinIT *pit = new PinIT(QStringList() << workFilePath, this);
        pit->exec();
    }
}

void coreimage::on_slideShow_clicked(bool checked)
{
    if (checked) {
        if (!slideShowTimer) {
			slideShowTimer = new QTimer();

			// switch to the next image when timeout
            connect(slideShowTimer, &QTimer::timeout, this, &coreimage::nextItem);
		}

        slideShowTimer->start(3000);
        ui->sideView->setVisible(false);

        if (ui->menu->isChecked()) {
            ui->menu->setChecked(false);
        }

        slideShow = true;
    } else {
        if (slideShowTimer) {
			delete slideShowTimer;
			slideShowTimer = nullptr;
            slideShow = false;
		}
	}

    this->setWindowState(this->windowState() ^ Qt::WindowFullScreen);
}

void coreimage::previousItem()
{
    if (imagesList.length() <= 1) {
        return;
    }

    // first do this to get all the space for contents
    if (uiMode == 2){
        ui->sideView->setVisible(0);
    }

	int index = imagesList.indexOf(workFilePath);
	int newIndex = index - 1;

	/* Loop through all images, so that we can load a proper one */
	while (newIndex != index) {
		workFilePath = imagesList.value(newIndex);

		if (!workFilePath.length() or !loadImage(workFilePath)) {
			newIndex--;

			if (newIndex < 0) {
				newIndex = imagesList.length() - 1;
			}
		} else {
            index = newIndex;
		}
    }
}

void coreimage::nextItem()
{
    if (imagesList.length() <= 1) {
        return;
    }

    // first do this to get all the space for contents
    if (uiMode == 2){
        ui->sideView->setVisible(0);
    }

	int index = imagesList.indexOf(workFilePath);
    // qDebug() << "current index"<< imagesList.indexOf(workFilePath);
	int newIndex = index + 1;

	/* Loop through all images, so that we can load a proper one */
	while (newIndex != index) {
		workFilePath = imagesList.value(newIndex);

		if (!workFilePath.length() or !loadImage(workFilePath)) {
			newIndex++;

			if (newIndex >= imagesList.length()) {
				newIndex = 0;
			}
		} else {
            index = newIndex;
		}
    }
}

void coreimage::on_cAlternateZoom_clicked()
{
    if(ui->viewWidget->zoomFactor() != 1){
        ui->viewWidget->zoomNormal();
        initialZoomMode = 1;
        smi->setValue( "CoreImage", "InitialZoomMode", initialZoomMode );
    } else {
        ui->viewWidget->zoomFit();
        initialZoomMode = 0;
        smi->setValue( "CoreImage", "InitialZoomMode", initialZoomMode );
    }
}

void coreimage::openFileDialog()
{
    // see if we have opened files, if so then set the folder path from the file
    QString openFrom = QDir::homePath();
    if (workFilePath.length()) {
        QFileInfo fileInfo(workFilePath);

        if (fileInfo.exists()) {
            openFrom =  workFilePath ;
            workFilePath.clear();
        }
    }

    QString filePath = QFileDialog::getOpenFileName(this->parent() ? this->parentWidget() : this, "Open image", openFrom, openFilter);

    if (!filePath.isEmpty()) {
        imagesList.clear();
        workFilePath = filePath;
        getImagesForOpenedImage();
        loadImage(workFilePath);
        enableButtons(true);
    }
}

void coreimage::showSideView()
{
    if (ui->sideView->isVisible()) {
        ui->sideView->setVisible(false);
    } else {
        ui->sideView->setVisible(true);
    }
}

void coreimage::changeZoomLabel()
{
    zoomLbl->clear();
    zoomLbl->setText("Zoom: " + QString::number(ui->viewWidget->zoomFactor(), 'f', 2) + "x");
}

void coreimage::closeSlideShow()
{
    if (slideShow) {
        on_slideShow_clicked(false);
        ui->slideShow->setChecked(false);
    }
}

void coreimage::on_activitiesList_itemDoubleClicked(QListWidgetItem *item)
{
    QString filePath = item->data(Qt::UserRole).toString();
    QFileInfo fileInfo(filePath);

    if (not fileInfo.exists()) {
        CPrime::MessageEngine::showMessage("org.cubocore.CoreImage", "CoreImage", "Empty file name!!!", "Select a valid file");
        return;
    } else {
        imagesList.clear();
        workFilePath = filePath;
        getImagesForOpenedImage();
        loadImage(filePath);
        enableButtons(true);
    }
}


void coreimage::closeEvent(QCloseEvent *event)
{
    event->ignore();

    qDebug()<< "save window stats"<< this->size() << this->isMaximized();
    smi->setValue("CoreImage", "WindowSize", this->size());
    smi->setValue("CoreImage", "WindowMaximized", this->isMaximized());

    if (QFileInfo::exists(workFilePath) && activities) {
        CPrime::ActivitiesManage::saveToActivites("coreimage", QStringList()<< workFilePath);
    }
    event->accept();
}
