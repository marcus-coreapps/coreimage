/*
    *
    * This file is a part of CoreImage.
    * An image viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#ifndef COREIMAGE_H
#define COREIMAGE_H

#include <QWidget>
#include <QImage>
#include <QLabel>
#include <QFileDialog>
#include <QCloseEvent>

#include <cprime/messageengine.h>

#include "settings.h"


class QListWidgetItem;
class QSpacerItem;
class QTimer;
class QScrollBar;

namespace Ui {
	class coreimage;
}

class coreimage : public QWidget {

	Q_OBJECT

public:
	explicit coreimage(QWidget *parent = nullptr);
	~coreimage();

	void sendFiles(const QStringList &paths);
    bool loadImage(QString filePath);

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::coreimage   *ui;
    QLabel          *zoomLbl;
    QString         workFilePath, saveFilter, openFilter ;
    bool            sideView, slideShow, activities, disableTrashConfirmationMessage, initialZoomMode, ascending, windowMaximized;
    int             uiMode, sortMode;
    QSize           toolsIconSize, listViewIconSize, windowSize;
    QTimer          *slideShowTimer;
    QStringList     imagesList;
    settings        *smi;

    void loadSettings();
    void startMode();
    void startSetup();
    void setupIcons();
    void shortcuts();
    void getImageFilters();
    void enableButtons(bool enable);
    void getImagesForOpenedImage();
    void loadActivities();

    void setSortMode();

private slots:
    void on_cSave_clicked();
    void on_cSaveAs_clicked();
    void on_rotate_clicked();
    void on_openInEditor_clicked();
    void on_openmetadataApp_clicked();
    void on_containingFolder_clicked();
    void on_cTrashIt_clicked();
    void on_shareIt_clicked();
    void on_pinIt_clicked();
    void on_slideShow_clicked(bool checked);
    void previousItem();
    void nextItem();
    void openFileDialog();
    void showSideView();
    void changeZoomLabel();
    void closeSlideShow();
    void on_activitiesList_itemDoubleClicked(QListWidgetItem *item);
    void on_cAlternateZoom_clicked();


private Q_SLOTS:
    void nextSortMode();
};

#endif // COREIMAGE_H
