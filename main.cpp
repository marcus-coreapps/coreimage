/*
    *
    * This file is a part of CoreImage.
    * An image viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QApplication>
#include <QFileInfo>
#include <QCommandLineParser>

#include "coreimage.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CoreImage");
    app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
    app.setDesktopFileName("org.cubocore.CoreImage.desktop");
    app.setQuitOnLastWindowClosed(true);

    QCommandLineParser parser;
	parser.setApplicationDescription("An image viewer from C Suite");
    parser.addHelpOption();
    parser.addVersionOption();
	parser.addPositionalArgument("files", "Image files that you want to view.");
    parser.process(app);

    QStringList args = parser.positionalArguments();
    QStringList paths;

	Q_FOREACH (QString arg, args) {
        QFileInfo info(arg);
        paths.push_back(info.absoluteFilePath());
    }

    coreimage e;
    if (paths.count()) {
        e.sendFiles(paths);
    }
    e.show();
    // QCoreApplication::processEvents();

    return app.exec();
}
