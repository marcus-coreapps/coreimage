/*
    *
    * This file is a part of CoreImage.
    * An image viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QImageReader>
#include <QImageWriter>
#include <QFileInfo>

#include "view.h"

view::view(QWidget *parent) : QGraphicsView(parent)
{
    setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    setCacheMode(QGraphicsView::CacheBackground);
    setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    setDragMode(QGraphicsView::ScrollHandDrag);

    image = new QGraphicsPixmapItem();
    image->setTransformationMode(Qt::SmoothTransformation);

    mScene = new QGraphicsScene();
    mScene->addItem(image);

    setScene(mScene);

//    qDebug() << "cons" << size();
}

bool view::loadImage(QString fileName)
{
//    qDebug() << "QGraphicsView size" << size() << "QGraphicsScene size"<< mScene->sceneRect();
    QByteArray format = QImageReader::imageFormat(fileName);
    if (not QImageReader::supportedImageFormats().contains(format))
        return false;

    mFileName = fileName;

    QPixmap pix(fileName);

    if (pix.isNull()) {
        return false;
    }

    image->setPixmap(pix);
    mScene->setSceneRect(image->boundingRect());

//    qDebug() << "QGraphicsView size" << size()<< "QGraphicsScene size"<< mScene->sceneRect();
    return true;
}

void view::clear()
{
	/** Remove image from the scene */
	mScene->removeItem( image );

	/** Delete it and set it null pointer */
	delete image;

	/** Recreate this iamge object */
	image = new QGraphicsPixmapItem();
	image->setTransformationMode(Qt::SmoothTransformation);

	/** Add it to the scene */
	mScene->addItem(image);

    isFit = false;
}

QImage view::asImage()
{
	return image->pixmap().toImage();
}

qreal view::zoomFactor() {

	return mScale;
}

void view::zoomIn()
{
    int static zoomInLimit = 10.00;
    if (mScale < zoomInLimit) {
        isFit = false;

        scale(scaleFactor, scaleFactor);
        mScale *= scaleFactor;
        emit zoomChanged();
    }
}

void view::zoomOut()
{
    double static zoomOutLimit = 0.05;
    if (mScale > zoomOutLimit) {
        isFit = false;

        scale(invScaleFactor, invScaleFactor);
        mScale *= invScaleFactor;
        emit zoomChanged();
    }
}

void view::zoomNormal()
{
    resetTransform();
    isFit = false;
    mScale = 1.0;

    emit zoomChanged();
}

void view::zoomFit()
{
    fitInView(image, Qt::KeepAspectRatio);
	isFit = true;
	mScale = transform().m11();

    emit zoomChanged();
}

void view::rotateLeft()
{
	QTransform tran;
    tran.rotate(-90);

    mScene->removeItem(image);
    image->setPixmap(image->pixmap().transformed(tran, Qt::SmoothTransformation));
    mScene->addItem(image);
    mScene->setSceneRect(image->boundingRect());

    if (isFit) {
		zoomFit();
    }
}

void view::rotateRight()
{
	QTransform tran;
    tran.rotate(90);

    mScene->removeItem(image);
    image->setPixmap(image->pixmap().transformed(tran, Qt::SmoothTransformation));
    mScene->addItem(image);
    mScene->setSceneRect(image->boundingRect());

    if (isFit) {
		zoomFit();
    }
}

bool view::saveImage(QString fileName)
{
    if (fileName.length()) {
		mFileName = fileName;
    }

    return image->pixmap().save(mFileName);
}

void view::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        if (isFit) {
			zoomNormal();
        } else {
			zoomFit();
        }
    }
}

void view::wheelEvent(QWheelEvent *event)
{
	QPoint wheelScroll = event->angleDelta();

	if ((event->buttons() & Qt::RightButton) // Mouse right button + wheel zoom
			|| (event->modifiers() & Qt::ControlModifier)) { // Ctrl + wheel zoom
		if ((wheelScroll.x() > 0) || (wheelScroll.y() > 0)) {
			zoomIn();
		} else {
			zoomOut();
		}
	} else if (!event->buttons()) { // Navigation of images
		if ((wheelScroll.x() > 0) || (wheelScroll.y() > 0)) {
            emit prevImage();
		} else {
            emit nextImage();
		}
    } else {
        QGraphicsView::wheelEvent(event);
    }
}

void view::resizeEvent(QResizeEvent *event) {
    event->accept();

    /** Showing the image for the first time */
    if ( mScale < 0 ) {
        mScale = 1.0;

        resetTransform();
    }

    else {
        if ( isFit ) {
            zoomFit();
        }
    }
}
